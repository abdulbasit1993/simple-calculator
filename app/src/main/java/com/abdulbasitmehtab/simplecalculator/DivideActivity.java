package com.abdulbasitmehtab.simplecalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class DivideActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText e1, e2;
    TextView result;
    Button btDivide;
    double a, b, div;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_divide);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Divide");
        e1 = (EditText) findViewById(R.id.etNum1);
        e2 = (EditText) findViewById(R.id.etNum2);
        result = (TextView) findViewById(R.id.tvResult);
        btDivide = (Button) findViewById(R.id.btDivide);

        btDivide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a = Double.parseDouble(e1.getText().toString());
                b = Double.parseDouble(e2.getText().toString());
                div = a / b;
                result.setText(Double.toString(div));
            }
        });
    }
}