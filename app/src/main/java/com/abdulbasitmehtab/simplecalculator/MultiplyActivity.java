package com.abdulbasitmehtab.simplecalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MultiplyActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText e1, e2;
    TextView result;
    Button btMult;
    double a, b, mul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiply);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Multiply");
        e1 = (EditText) findViewById(R.id.etNum1);
        e2 = (EditText) findViewById(R.id.etNum2);
        result = (TextView) findViewById(R.id.tvResult);
        btMult = (Button) findViewById(R.id.btMult);

        btMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a = Double.parseDouble(e1.getText().toString());
                b = Double.parseDouble(e2.getText().toString());
                mul = a * b;
                result.setText(Double.toString(mul));
            }
        });
    }
}
